/*
   Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

   utility to convert unix time representation to human readable
   format. It looks for lines starting with a number or a prefix and
   then a number, where a prefix is either "time:" or "tm:".
*/

%{

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#include <boost/date_time/time_zone_base.hpp>
#include <boost/date_time/local_time/local_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/optional.hpp>

#include <bb/core/date.h>
#include <bb/core/ptime.h>
#include <bb/core/timeval.h>
#include <bb/core/program_options.h>

using namespace boost::local_time;
using namespace boost::posix_time;

bool global = false; // true if we want to match more than just the first time string.
bool full_date = false; // output YYYYMMDD as well as time
std::string start_time_str;
bb::timeval_t start_time = bb::timeval_t::earliest;
bool use_block_buffering = false;
std::string otimezone;

void convert(const char *prefix, unsigned int prefix_len, bool start_of_line, const char *s, size_t s_len);

void setup_buffering()
{
    if (!use_block_buffering)
    {
        // can't call yy_set_interactive unless a buffer exists first
        yy_switch_to_buffer(yy_create_buffer(stdin, YY_BUF_SIZE));
        yy_set_interactive(true);
    }
}

%}

NEWLINE         [\n]
TIME            (([[:digit:]]{9,10})(("."([[:digit:]]{3,9}))?))/[^[:digit:].]
THIS_ERA        [12]([[:digit:]]{9})(("."([[:digit:]]{3,9}))?)/[^[:digit:].]
JSON_TIME       [12]([[:digit:]]{15})/[^[:digit:].]

%%

^{TIME}          convert("",      0, true,  yytext, yyleng);
^"time:"{TIME}   convert("time:", 5, true,  yytext, yyleng);
^"tm:"{TIME}     convert("tm:",   3, true,  yytext, yyleng);
"time:"{TIME}    convert("time:", 5, false, yytext, yyleng);
"tm:"{TIME}      convert("tm:",   3, false, yytext, yyleng);
"ts=\""{TIME}    convert("ts=\"", 4, true,  yytext, yyleng);
[^[:digit:].\n]{THIS_ERA} convert(yytext,  1, false, yytext, yyleng);
": "{JSON_TIME}  convert(": ",    2, false, yytext, yyleng);
{NEWLINE}        std::cout << '\n';
<<EOF>>          exit(EXIT_SUCCESS);
.                std::cout.write(yytext, yyleng);

%%

inline int is_digit(char c)
{
    return c >= '0' && c <= '9';
}

const unsigned int bufsize = 32;
char *numer = new char[bufsize];

const unsigned int rhs_len = 9; // nanosecond precision
char *denom = new char[rhs_len + 1];

void convert(const char *prefix, unsigned int prefix_len, bool start_of_line, const char *s, size_t s_len)
{
    memset(denom, '0', 10);
    if (!global && !start_of_line)
    {
        std::cout.write(s, s_len);
        return;
    }

    s += prefix_len;

    unsigned int p;
    unsigned dec_digits = 0;
    for (p = 0; is_digit(*s) && p < bufsize-1; s++, p++)
        numer[p] = *s;
    numer[p] = '\0';

    bool is_json_time = (p == 16); // e.g. 1287604789354099
    if (is_json_time)
    {
        dec_digits = 6;
        memcpy(denom, &numer[10], 6);
        memset(denom + 6, '0', 3); // pad with ASCII zeros
        numer[10] = '\0';
        denom[9] = '\0';
    }
    else if (*s == '.')
    {
        s++;
        for (p = 0; is_digit(*s) && p < bufsize-1 && p < rhs_len; s++, p++)
            denom[p] = *s;
        dec_digits = p;
        for ( ; p < rhs_len; p++) // pad zeros on rhs if needed
            denom[p] = '0';
        denom[p] = '\0';
    }

    bb::timeval_t tv(bb::timeval_t::from_sec_nsec(atoi(numer), atoi(denom)));
    bb::date_t date(tv);
    std::cout.write(prefix, prefix_len);

    if (!otimezone.empty())
    {
        time_zone_ptr zone(new posix_time_zone(otimezone));
        local_date_time localt(from_time_t(date.sec()), zone);

        tm tmobj = to_tm(localt.local_time());
        date = bb::timeval_t::from_sec_nsec(mktime(&tmobj), date.nsec());
    }

    if (is_json_time)
        std::cout.put('\"');

    if (full_date)
    {
        std::cout << date.toString(true);
    }
    else if( start_time != bb::timeval_t::earliest )
    {
        std::cout << bb::timeval_diff( tv, start_time );
    }
    else
    {
        static const unsigned powers[10] = { 0, 100000000u, 10000000u, 1000000u, 100000u, 10000u, 1000u, 100u, 10u, 1u };

        std::cout
            << std::setw(2) << date.hour() << ':'
            << std::setw(2) << date.min () << ':'
            << std::setw(2) << date.sec ();
        if (dec_digits > 0)
            std::cout << '.' << std::setw(dec_digits) << date.nsec() / powers[dec_digits];
    }

    if (is_json_time)
        std::cout.put('\"');
}

void print_usage(std::ostream& os, const boost::program_options::options_description& desc)
{
    os << "usage: tf [options]" "\n"
        "\n"
        " tf translates an occurence of a timeval in its input into a human readable" "\n"
        " time string (HH:MM:SS.USEC or YYYY-MM-DD HH:MM:SS.USEC, depending on the options)." "\n"
        "\n"
        " It makes a best effort guess as to what constitutes a timeval. Some examples:" "\n"
        "     - numbers at the beginning of a line or preceeded by 'time:' or 'tm:' that" "\n"
        "       are 9 or 10 digits, plus an optional period and 4 or 6 digits" "\n"
        "     - numbers anywhere that are 10 digits, start with '1' or '2', plus an" "\n"
        "       optional period and 4 or 6 digits." "\n"
        "\n"
        << desc;
}

int main(int argc, char **argv)
try
{
    namespace po = boost::program_options;

    bb::options_description opts("options");
    opts.add_options()
        ("global"        ",g", po::bool_switch( &global              ), "global match; replaces all time occurences in input")
        ("date"          ",d", po::bool_switch( &full_date           ), "displays full date rather than just time")
        ("block-buffer"  ",b", po::bool_switch( &use_block_buffering ), "use block buffering (default is line buffering)")
        ("time-zone"     ",z", po::value      ( &otimezone           ), "use 'tz' timezone, i.e. UTC+8")
        ("duration-from" ",D", po::value      ( &start_time_str      ), "print deltas since this start-time")
        ("help",                                                       "display this help")
    ;
    po::positional_options_description pos;
    pos.add("file", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(opts).positional(pos).run(), vm);
    po::notify(vm);

    if (vm.count("help"))
    {
        print_usage(std::cout, opts);
        return EXIT_SUCCESS;
    }

    if (vm.count("file")) // input filenames are not yet supported
    {
        print_usage(std::cerr, opts);
        return EXIT_FAILURE;
    }

    if( vm.count("duration-from") )
    {
        if( start_time_str.find('.') == std::string::npos )
        { //no decimal. bson style timeval
            start_time = bb::timeval_t::from_microsec( atol( start_time_str.c_str() ) );
        }
        else
        { //decimal. standard timeval
            start_time = bb::timeval_t( start_time_str.c_str() );
        }
    }

    std::cout << std::setfill('0');
    setup_buffering();
    yylex();
}
catch (const std::exception& ex)
{
    std::cerr << "error: " << ex.what() << std::endl;
    return EXIT_FAILURE;
}
